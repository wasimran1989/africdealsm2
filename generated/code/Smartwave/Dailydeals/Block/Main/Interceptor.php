<?php
namespace Smartwave\Dailydeals\Block\Main;

/**
 * Interceptor class for @see \Smartwave\Dailydeals\Block\Main
 */
class Interceptor extends \Smartwave\Dailydeals\Block\Main implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Block\Product\Context $context, \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Framework\Data\Helper\PostHelper $postDataHelper, \Magento\Catalog\Model\Layer\Resolver $layerResolver, \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository, \Magento\Framework\Url\Helper\Data $urlHelper, \Smartwave\Dailydeals\Model\DailydealFactory $dailydealFactory, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $productFactory, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $dailydealFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage($product, $imageId, $attributes = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImage');
        if (!$pluginInfo) {
            return parent::getImage($product, $imageId, $attributes);
        } else {
            return $this->___callPlugins('getImage', func_get_args(), $pluginInfo);
        }
    }
}
